
import express from "express";
import cors from 'cors';
import { categoriesController } from "./controller/categories-controller";
import { operationsController } from "./controller/operations-controller";



export const server = express();

server.use(express.json());
server.use(cors());


server.use('/api/budget/categories',categoriesController);
server.use('/api/budget/operations',operationsController);

