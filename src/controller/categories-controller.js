import { Router } from "express";

import {CategorieRepository } from "../repository/CategorieRepo";

export const categoriesController = Router();

//======================================== Router FindAll ok =======================================//
    
categoriesController.get("/", async (req, res)=>{
    try {
            let data = await new CategorieRepository().findAll();
            res.json(data);
            res.end();
        } catch (error) {
            console.log(error);
            res.status(500).json({
                message: 'Server Error'
            })
        }
    })
    


// //======================================== Router FindById OK ====================================//


    
categoriesController.get('/:id', async (req, res)=>{
    try {
        let data = await new CategorieRepository().findById(req.params.id);
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


//========================================  Router add ok   ====================================//



categoriesController.post('/', async (req, res)=>{
    try {        
       
        let toAdd = req.body;     
        await new CategorieRepository().add(req.body);
        res.status(201).json(toAdd);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


//========================================  Router update   ok ====================================//


categoriesController.put('/', async (req, res)=>{
    try {
        await new CategorieRepository().update(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


// //========================================  Router Delete ok  ====================================//



categoriesController.delete('/:id', async (req, res)=>{
    try {
        await new CategorieRepository().delete(req.params.id);
        res.end();
    } catch (error) {
        res.status(500).json({
            message: 'Server Error'
        })
    }
})
