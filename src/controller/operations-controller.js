import { Router } from "express";

import {OperationRepository } from "../repository/OperationRepo";

export const operationsController = Router();

//======================================== Router FindAll =======================================//
    
// operationsController.get("/", async (req, res)=>{
//     try {
//             let data = await new OperationRepository().findAll();
//             res.json(data);
//             res.end();
//         } catch (error) {
//             console.log(error);
//             res.status(500).json({
//                 message: 'Server Error'
//             })
//         }
//     })
    
operationsController.get('/', async (req, resp) => {
        try {
            let data;
            if(req.query.search) {
                data = await new  OperationRepository().search(req.query.search);
            }  else {
                data= await new OperationRepository().findAll();
            }
            resp.json(data);
        }catch(error) {
            console.log(error);
            resp.status(500).json(error);
        }
    });

    

//======================================== Router findAllCategorieId =======================================//

    
/**
 * find data by user_id
 */
 operationsController.get('/categories/:id', async (req, res) => {
    try {
        let data = await new OperationRepository().findAllCategorieId(req.params.id);
        if (data == null) {
            res.status(404).end();
            return
        }
        res.json(data);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});




    operationsController.get("/test", async (req, res)=>{
        try {
                let data = await new OperationRepository().findKey();
                res.json(data);
                res.end();
            } catch (error) {
                console.log(error);
                res.status(500).json({
                    message: 'Server Error'
                })
            }
        })

//======================================== Router FindById OK ====================================//
        
    
    operationsController.get("/month/:date", async (req, res)=>{
        try {
                let data = await new OperationRepository().findByDate(req.params.date);
                res.json(data);
                res.end();
            } catch (error) {
                console.log(error);
                res.status(500).json({
                    message: 'Server Error'
                })
            }
        })

//======================================== Router FindById OK ====================================//
    
operationsController.get('/:id', async (req, res)=>{
    try {
        let data = await new OperationRepository().findById(req.params.id);
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(404).end();
        
    }
})


//========================================  Router add ok  ====================================//

operationsController.post('/', async (req, res)=>{
    try {   
        let toAdd = req.body;     
        await new OperationRepository().add(toAdd);
        res.status(201).json(toAdd);

    
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})



// operationsController.patch('/:id', async (req,res)=>{
//     try {
//         let data = await new OperationRepository().findById(req.params.id);
//         if (!data) {
//             res.send(404).end();
//             return
//         }
//         let update = {...data, ...req.body};
//         await new OperationRepository().update(update);
//         res.json(update);
//     } catch (error) {
//         console.log(error);
//         res.status(400).end();
//     }
// })




//========================================  Router update   ok ====================================//



operationsController.put('/', async (req, res)=>{
    try {
        await new OperationRepository().update(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


//========================================  Router Delete ok  ====================================//



operationsController.delete('/:id', async (req, res)=>{
    try {
        await new OperationRepository().delete(req.params.id);
        res.end();
    } catch (error) {
        res.status(500).end()
    }
})

    