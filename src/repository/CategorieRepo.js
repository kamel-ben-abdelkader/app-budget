import {Categorie} from '../entity/categorie'
import {connection} from './bddConnect';
 
 
export class CategorieRepository
{
 


//======================================== Method FindAll ====================================//

    /**
     * Find every rows in categories table
     * @returns 
     */
        async findAll() {
 
 
       const [rows] = await connection.execute('SELECT * FROM categories ');
       const categories = [];
       for (const row of rows) {
           let categorie = new Categorie (row.nom,row.id);
           categories.push(categorie);
  
       }
       return categories;
   }




//======================================== Method FindById ====================================//

    

    /**
     * Find a row by his id in the categorie table
     * @param {*} id 
     * @returns 
     */

    
     async findById(id) {
        const [rows] = await connection.execute(`SELECT * FROM categories WHERE Id=?`, [id]);
        let categorieId = [];
        for (const row of rows) {
            let instance = new Categorie (row.nom,row.id);
            categorieId.push(instance);
        }
        return categorieId
    }


//======================================== Method Add ====================================//


     /**
     * add a row in the categorie table via an object
     * @param {Object} categorie
     */

    async add(categorie) {
        const [rows] = await connection.execute(`INSERT INTO categories (nom) VALUES(?)`, [categorie.nom]);
    };



//======================================== Method Update ====================================//


   
    async update(categorie) {
        const [rows] = await connection.execute(`UPDATE categories SET nom=? WHERE id=?`, [categorie.nom, categorie.id]);
    };





//======================================== Method Delete ====================================//


 
    async delete(id) {
        const [rows] = await connection.execute(`DELETE FROM categories WHERE id=?`, [id]);
    };


  
 }
 



