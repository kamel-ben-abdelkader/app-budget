import { Operation } from '../entity/Operation';
import { connection } from './bddConnect';
import { Categorie } from './../entity/categorie'

export class OperationRepository {



    //======================================== Method FindAll ok ====================================//

    /**
     * Find every rows in operations table
     * @returns 
     */
    async findAll() {


        const [rows] = await connection.execute({ sql: 'SELECT * FROM operations LEFT JOIN categories ON operations.CategorieId = categories.id', nestTables: true });
        console.log(rows);
        const operations = [];
        for (const row of rows) {
            let operation = new Operation(row.operations.description, row.operations.date, row.operations.montant, row.operations.CategorieId, row.operations.id);
            operation.categorie = new Categorie(row.categories.nom, row.categories.id)

            // objet dans operations  et metre objet categorie. operation.categorie.nom
            operations.push(operation);

        }
        //    console.log(operations);
        return operations;


    }

    //======================================== Method Search ====================================//



    static async search(term) {
        const [rows] = await connection.query('SELECT * FROM  operations WHERE CONCAT(description,date,montant) LIKE ?', ['%' + term + '%'])
        return rows.map(row => new Operation(row['description'], row['date'], row['montant'], row['CategorieId'], row['id']));

    }



    //======================================== Method FindByDate====================================//


    async findByDate(date) {


        const [rows] = await connection.execute({ sql: 'SELECT * FROM operations LEFT JOIN categories ON operations.CategorieId = categories.id WHERE MONTH(date)=?', values: [date], nestTables: true });
        console.log(rows);
        const operations = [];
        for (const row of rows) {
            let operation = new Operation(row.operations.description, row.operations.date, row.operations.montant, row.operations.CategorieId, row.operations.id);
            operation.categorie = new Categorie(row.categories.nom, row.categories.id)


            operations.push(operation);

        }
        //    console.log(operations);
        return operations;


    }

    //======================================== Method FindById ok ====================================//



    /**
     * Find a row by his id in the operations table
     * @param {*} id 
     * @returns 
     */


    async findById(id) {
        const [rows] = await connection.execute(`SELECT * FROM operations WHERE Id=?`, [id]);
        let operationId = [];
        for (const row of rows) {
            let instance = new Operation(row.description, row.date, row.montant, row.categorieId);
            operationId.push(instance);
        }
        return operationId;
    }


    //======================================== Method Add  NO ===================================//


    /**
    * add a row in the operation table via an object
    * @param {Object} operation
    */

    async add(op) {
        const [rows] = await connection.execute(`INSERT INTO operations (description, date, montant, CategorieId) VALUES(?, ?, ?, ?)`, [op.description, op.date, op.montant, op.categorieId]);
        op.id = rows.insertId;


    }



    //======================================== Method Update ====================================//



    async update(op) {
        const [rows] = await connection.execute(`UPDATE operations SET description=?, date=?, montant=?, CategorieId=? WHERE id=?`, [op.description, op.date, op.montant, op.CategorieId, op.id]);
    };





    //======================================== Method Delete ====================================//



    async delete(id) {
        const [rows] = await connection.execute(`DELETE FROM operations WHERE id=?`, [id]);
    };



}



// //======================================== Method Findallby categorie Id ok ====================================//




// async findAllCategorieId(id) {
//     let [rows] = await connection.execute('SELECT * FROM operations WHERE CategorieId=?', [id]);
//     let operation = [];
//     for (const row of rows) {
//         let instance = new Operation(row.description, row.date,row.montant,row.categorieId);
//         operation.push(instance);
//     }
//     if (operation.length != 0) {
//         return operation;
//     }
//     return null
// }



// //======================================== Method FindByForeignKey test ====================================//

// async findKey() {


//     const [rows] = await connection.execute('SELECT CategorieId, categories.nom FROM operations, categories WHERE CategorieId = categories.id');
//     console.log(rows);
//     const operationsKey = [];
//     for (const row of rows) {
//         let operation = new Categorie(row.id,row.nom); 


//         // objet dans operations  et metre objet categorie.
//         operationsKey.push(operation);

//     }
//     console.log(operationsKey);
//     return operationsKey;
// }
