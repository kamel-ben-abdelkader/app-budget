export class Operation{
    
    id;
    description;
    date;
    montant;
    categorieId;
    categorie;
 /**
  * 
  * @param {String} paramDesc 
  * @param {Date} paramDate 
  * @param {Number} paramMontant 
  * @param {Number} paramCategorieId 
  * @param {Number} paramId 
  */

    constructor(paramDesc, paramDate, paramMontant, paramCategorieId, paramId) {
        this.description = paramDesc;
        this.date = paramDate;
        this.montant = paramMontant;
        this.categorieId = paramCategorieId;
        this.id = paramId;
}

}