


DROP TABLE IF EXISTS categories;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
`nom` varchar(252) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4;

INSERT INTO
  categories (nom)
VALUES
  ('Alimentation'),('Salaires'),('Logement'),('Santé'),('Shopping'),('Sortie'),('Voiture'),('Vacances');


DROP TABLE IF EXISTS operations;
CREATE TABLE `operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(252) NOT NULL,
  `date` DATE NOT NULL,
  `montant` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  `CategorieId` int,
  KEY `FK_CatOperation` (`CategorieId`),
  CONSTRAINT `FK_CatOperation` FOREIGN KEY (`CategorieId`) REFERENCES `categories`(`id`) ON DELETE
  SET
    NULL ON UPDATE
  SET
    NULL
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4;




INSERT INTO
  operations (description, date, montant, CategorieId)
VALUES
  ('Paie du mois de Juillet', '2021-07-01', 1500.00, 2),('Menu Mac Donald', '2021-07-02', -7, 1),('Loyer', '2021-07-05', -450, 3);
